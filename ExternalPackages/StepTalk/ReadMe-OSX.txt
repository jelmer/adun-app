StepTalk
--------

Ahthor: Stefan Urbanek
License: LGPL

What is StepTalk ?
------------------

StepTalk is a scripting framework for creating scriptable servers or
applications. StepTalk, when combined with the dynamism that the Objective-C
language provides, goes way beyond mere scripting.  

What is StepTalking ?
---------------------

StepTalking is an application for "talking to objects" in StepTalk languages.  

Installation
------------

1. Copy StepTalk.framework into ~/Library/Frameworks or /Library/Frameworks
2. Copy StepTalking application wherever you like, preferably to the 
   Applications folder

Feedback
--------
Send comments, questions and suggestions to: stefan@agentfarms.net
